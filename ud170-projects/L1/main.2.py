# Second iteration
# This file has the fixed version of within_week
qkey = "account_key"

# CSVs in Python
import unicodecsv


def read_csv(filename):
    with open(filename, "rb") as f:
        reader = unicodecsv.DictReader(f)
        return list(reader)


enrollments = read_csv("resources/enrollments.csv")
daily_engagement = read_csv("resources/daily_engagement.csv")
project_submissions = read_csv("resources/project_submissions.csv")

# Fixing Data Types

from datetime import datetime as dt


def parse_date(date):
    if date == '':
        return None
    else:
        return dt.strptime(date, '%Y-%m-%d')


def parse_maybe_int(i):
    if i == '':
        return None
    else:
        return int(i)


for enrollment in enrollments:
    enrollment['cancel_date'] = parse_date(enrollment['cancel_date'])
    enrollment['days_to_cancel'] = parse_maybe_int(
        enrollment['days_to_cancel'])
    enrollment['is_canceled'] = enrollment['is_canceled'] == 'True'
    enrollment['is_udacity'] = enrollment['is_udacity'] == 'True'
    enrollment['join_date'] = parse_date(enrollment['join_date'])

print enrollments[0]
for engagement_record in daily_engagement:
    engagement_record['lessons_completed'] = int(
        float(engagement_record['lessons_completed']))
    engagement_record['num_courses_visited'] = int(
        float(engagement_record['num_courses_visited']))
    engagement_record['projects_completed'] = int(
        float(engagement_record['projects_completed']))
    engagement_record['total_minutes_visited'] = float(
        engagement_record['total_minutes_visited'])
    engagement_record['utc_date'] = parse_date(engagement_record['utc_date'])

print daily_engagement[0]
for submission in project_submissions:
    submission['completion_date'] = parse_date(submission['completion_date'])
    submission['creation_date'] = parse_date(submission['creation_date'])

print project_submissions[0]

# Investigating the Data


def getUnique(list, key="account_key"):
    uniques = set()
    for item in list:
        uniques.add(item[key])
    return uniques


enrollment_num_rows = len(enrollments)
enrollment_num_unique_students = len(getUnique(enrollments))
engagement_num_rows = len(daily_engagement)
engagement_num_unique_students = len(getUnique(daily_engagement, "acct"))
submission_num_rows = len(project_submissions)
submission_num_unique_students = len(getUnique(project_submissions))

# Problems in the Data
for i, engagement in enumerate(daily_engagement):
    daily_engagement[i][qkey] = engagement["acct"]
print "Key of first daily_engagement entry:", daily_engagement[0][qkey]

# Missing Engagement Records
uniq_engagement_students = set()
for item in daily_engagement:
    uniq_engagement_students.add(item[qkey])

exceptions = [
    enrollment for enrollment in enrollments
    if enrollment[qkey] not in uniq_engagement_students
]
print exceptions[0]

# Checking for More Problem Records
for enrollment in exceptions:
    if enrollment["join_date"] != enrollment["cancel_date"]:
        print enrollment

# Tracking Down the Remaining Problems
udacity_test_accounts = set()
for enrollment in enrollments:
    if enrollment['is_udacity']:
        udacity_test_accounts.add(enrollment['account_key'])


def remove_udacity_accounts(data):
    non_udacity_data = []
    for data_point in data:
        if data_point['account_key'] not in udacity_test_accounts:
            non_udacity_data.append(data_point)
    return non_udacity_data


non_udacity_enrollments = remove_udacity_accounts(enrollments)
non_udacity_engagement = remove_udacity_accounts(daily_engagement)
non_udacity_submissions = remove_udacity_accounts(project_submissions)

# Refining the Question

paid_students = {}
for enrollment in non_udacity_enrollments:
    if not enrollment["is_canceled"] or int(enrollment["days_to_cancel"]) > 7:
        key = enrollment[qkey]
        if key not in paid_students or enrollment["join_date"] > paid_students[key]:
            paid_students[key] = enrollment["join_date"]
print "Length paid_students:", len(paid_students)

# Getting Data from First Week


def remove_free_trail_students(data):
    return [i for i in data if i[qkey] in paid_students]


def within_week(engagement_record):
    engagement_date = engagement_record["utc_date"]
    join_date = paid_students[engagement_record[qkey]]
    time_delta = (engagement_date - join_date).days
    return time_delta < 7 and time_delta >= 0


paid_enrollments = remove_free_trail_students(non_udacity_enrollments)
paid_engagement = remove_free_trail_students(non_udacity_engagement)
paid_submissions = remove_free_trail_students(non_udacity_submissions)

# Number of Visits in the First Week (part1)
for i, e in enumerate(paid_engagement):
    paid_engagement[i]["has_visited"] = e["num_courses_visited"] > 0
# Number of Visits in the First Week (end)

paid_engagement_in_first_week = [
    engagement_record for engagement_record in paid_engagement
    if within_week(engagement_record)
]
print "Length paid_engagement_in_first_week:", len(
    paid_engagement_in_first_week)

# Exploring Student Engagement

from collections import defaultdict

engagement_by_account = defaultdict(list)
for engagement_record in paid_engagement_in_first_week:
    account_key = engagement_record['account_key']
    engagement_by_account[account_key].append(engagement_record)

total_minutes_by_account = {}
for account_key, engagement_for_student in engagement_by_account.items():
    total_minutes = 0
    for engagement_record in engagement_for_student:
        total_minutes += engagement_record['total_minutes_visited']
    total_minutes_by_account[account_key] = total_minutes

import numpy as np
total_minutes = total_minutes_by_account.values()


def log_single_data(header, values):
    print header
    print 'Mean:', np.mean(values)
    print 'Standard deviation:', np.std(values)
    print 'Minimum:', np.min(values)
    print 'Maximum:', np.max(values)


log_single_data("\nTotal minutes:", total_minutes)

# Debugging Data Analysis Code

# reminder: https://wiki.python.org/moin/HowTo/Sorting#line-31
outlier = max(total_minutes_by_account.items(), key=lambda x: x[1])[0]
outlier_engagement_records = [
    engagement for engagement in paid_engagement_in_first_week
    if engagement[qkey] == outlier
]
print "Outlier records:", len(outlier_engagement_records)
period = outlier_engagement_records[-1]["utc_date"] - \
    outlier_engagement_records[0]["utc_date"]
print "Period:", period.days+1, "days"

# Lessons Completed In First Week

total_lessons_by_account = {
    k: sum(e["lessons_completed"] for e in v) for k, v in engagement_by_account.items()
}
log_single_data("\nTotal lessons:", total_lessons_by_account.values())

# Number of Visits in the First Week (part 2)

total_days_by_account = {
    k: sum(e["has_visited"] for e in v) for k, v in engagement_by_account.items()
}
log_single_data("\nTotal days visited:", total_days_by_account.values())

# Splitting out Passing Students


def passed_project(submission, keys):
    k, r = submission["lesson_key"], submission["assigned_rating"]
    return k in keys and (r == "PASSED" or r == "DISTINCTION")


passed_subway_project = [
    s[qkey] for s in paid_submissions if passed_project(s, ["746169184", "3176718735"])
]
passing_engagement, non_passing_engagement = [], []
for e in paid_engagement_in_first_week:
    if e[qkey] in passed_subway_project:
        passing_engagement.append(e)
    else:
        non_passing_engagement.append(e)
print "Passing engagements:", len(passing_engagement)
print "Non-passing engagements:", len(non_passing_engagement)

# Comparing the Two Student Groups


def analyse_section(values):
    return [
        np.mean(values),
        np.std(values),
        np.min(values),
        np.max(values),
    ]


def analyse_data(values):
    return [analyse_section([v[i] for v in values]) for i in range(0, len(values[0]))]


def log_data(headers, analyzed):
    for i, h in enumerate(headers):
        print "\n", (h + ":")
        for j, div in enumerate(["Mean", "Standard deviation", "Minimum", "Maximum"]):
            print "\n", div + ":"
            for item in analyzed:
                print item[0] + ":", item[1][i][j]


passing_accounts = {e[qkey]: [
    total_minutes_by_account[e[qkey]],
    total_lessons_by_account[e[qkey]],
    total_days_by_account[e[qkey]]
] for e in passing_engagement}
non_passing_accounts = {e[qkey]: [
    total_minutes_by_account[e[qkey]],
    total_lessons_by_account[e[qkey]],
    total_days_by_account[e[qkey]]
] for e in non_passing_engagement}
titles = ["Total minutes", "Total lessons", "Total days"]
log_data(
    titles,
    [
        ("Passed student", analyse_data(passing_accounts.values())),
        ("Non-passed student", analyse_data(non_passing_accounts.values()))
    ]
)

# Making Histograms

import matplotlib.pyplot as plt

for i,t in enumerate(titles):
    plt.title(t)
    plt.hist([v[i] for v in passing_accounts.values()])
    plt.hist([v[i] for v in non_passing_accounts.values()])
    plt.show()