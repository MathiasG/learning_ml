# POP Project

Dit project zal bestaan uit 3 "bundels" van projecten bestaande uit PoCs & mini-projecten.

* Bundel 1: projecten van Intro to Machine Learning _(ud120-projects)_
* Bundel 2: projecten van Deep Learning _(ud730-projects)_
* Bundel 3: projecten van Intro to Data Analyses _(ud170-projects)_