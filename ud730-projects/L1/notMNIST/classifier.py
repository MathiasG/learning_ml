from __future__ import print_function

import os
import sys

import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

from notMNIST_downloader import main as downloader

dataset_file = "notMNIST.pickle"


def get_overlapping(raw_notMNIST):
    """
    Gets a dictionary containing the overlapping samples in each notMNIST dataset.
    """
    train_size = raw_notMNIST["train_dataset"].shape
    valid_resized = np.resize(raw_notMNIST["valid_dataset"], train_size)
    test_resized = np.resize(raw_notMNIST["test_dataset"], train_size)
    return {
        "valid_test": [
            sample
            for sample in raw_notMNIST["valid_dataset"] == raw_notMNIST["test_dataset"]
            if sample.all()
        ],
        "valid_train": [
            sample
            for sample in valid_resized == raw_notMNIST["train_dataset"]
            if sample.all()
        ],
        "test_train": [
            sample
            for sample in test_resized == raw_notMNIST["train_dataset"]
            if sample.all()
        ]
    }


def reshape(raw_notMNIST):
    """
    Reshapes without using PCA.
    """
    for k in raw_notMNIST.keys()[3:]:
        raw_notMNIST[k] = np.array(
            [np.reshape(sample, (sample.size)) for sample in raw_notMNIST[k]])
    return raw_notMNIST


def test_linear_clf(raw_notMNIST, samples):
    """
    Tests a linear classifier and returns the test prediction score.
    (No preprocessing applied.)
    """
    clf = LogisticRegression()
    clf.fit(raw_notMNIST["train_dataset"][:samples],
            raw_notMNIST["train_labels"][:samples])
    clf.predict(raw_notMNIST["test_dataset"])
    return clf.score(raw_notMNIST["test_dataset"],
                     raw_notMNIST["test_labels"])


def main():
    raw_notMNIST = reshape(pd.read_pickle(dataset_file))
    overlapping = get_overlapping(raw_notMNIST)
    print("comparer/overlap:")
    print(pd.Series({
        "valid_test": len(overlapping["valid_test"]),
        "valid_train": len(overlapping["valid_train"]),
        "test_train": len(overlapping["test_train"]),
    }))
    print("samples/score:")
    print(pd.Series({
        50: test_linear_clf(raw_notMNIST, 50),
        100: test_linear_clf(raw_notMNIST, 100),
        1000: test_linear_clf(raw_notMNIST, 1000),
        5000: test_linear_clf(raw_notMNIST, 5000)
    }))


if __name__ == "__main__":
    if not os.path.exists(dataset_file):
        downloader()
    main()
