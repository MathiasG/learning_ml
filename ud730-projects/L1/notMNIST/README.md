# notMNIST

This folder contains the first exercise of "Deep Learning".

## Scripts to run

Before any ML algorithm can work we need a dataset. This dataset can be downloaded and processed by running *notMNIST_downloader.py*.

To run a test to verify how well a simple canned solution from sklearn works: run *notMNIST.py*. This will run the downloader if it can't find a dataset, verify how much overlap is between the train, test and validation sets and runs a linear logistic regression algorithm on a number of samples.

## TLDR;

Run *notMNIST.py* and it will automatically download everything in order to test a simple canned solution from sklearn.