#!/usr/bin/python

import pickle
import sys
import matplotlib.pyplot
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit

def sortBy(key, reverse=False):
    cleaned_data_dict = {
        k: v for k, v in data_dict.items() if v[key] != "NaN"
    }
    return sorted(cleaned_data_dict.items(), key=lambda x: x[1][key], reverse=reverse)

# read in data dictionary, convert to numpy array
data_dict = pickle.load(
    open("../final_project/final_project_dataset.pkl", "r")
)

# Q1
total = sortBy("salary")[-1]
print "Highest salary:", total[0]

# Q2
data_dict.pop("TOTAL", 0)

# Q3
print "Biggest earners (salary):", [x for x,_ in sortBy("salary", reverse=True)[:4]]
print "Biggest earners (bonus):", [x for x,_ in sortBy("bonus", reverse=True)[:4]]

features = ["salary", "bonus"]
data = featureFormat(data_dict, features)

for point in data:
    salary = point[0]
    bonus = point[1]
    matplotlib.pyplot.scatter(salary, bonus)

matplotlib.pyplot.xlabel("salary")
matplotlib.pyplot.ylabel("bonus")
matplotlib.pyplot.show()
