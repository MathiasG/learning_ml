#!/usr/bin/python


def getSortedByErrors(predictions, ages, net_worths):
    errors = (net_worths - predictions)**2  # (difference of each item)**2
    data = zip(ages, net_worths, errors)  # to asked tuple
    # https://wiki.python.org/moin/HowTo/Sorting#line-31
    data = sorted(data, key=lambda x: x[2])  # sort by error
    return data


def outlierCleaner(predictions, ages, net_worths):
    """
        Clean away the 10% of points that have the largest
        residual errors (difference between the prediction
        and the actual net worth).

        Return a list of tuples named cleaned_data where 
        each tuple is of the form (age, net_worth, error).
    """

    cleaned_data = []

    # your code goes here

    data = getSortedByErrors(predictions, ages, net_worths)
    cleaned_data = data[:int(len(data)*.9)]  # take only last 90%

    return cleaned_data


def getErrors(predictions, ages, net_worths):
    data = getSortedByErrors(predictions, ages, net_worths)
    return data[int(len(data*.1)):]
