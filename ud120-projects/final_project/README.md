# Findings on Enron data analysis with ML

## Step by step

1. Tried using only financial data, only email data as well as both. _(best in order: email, both, finance)_ **Why did I take both?** Email contained 14 pois.
2. Filtered entries by missing data.
3. Tried both filtering the data until it was impossible to remove pois and removing pois as well in order to get a better score. (Removing pois looked promising, but in tester.py removing only non-pois had better results.)
4. Tried both removing possible static features (low data count) as keeping them. (Not removing them gave worse results and warnings.)
5. Tried removing features containing deferred & from/to messages as well as keeping them. Removing them resulted in noticeably better scores.
6. Tried different classifiers with default parameters in a grid search:
    * What I ran:
        * All classifiers from the classifier comparer example of sklearn.
    * What I didn't run:
        * Any clusters: this could help identify POIs as it's sole purpose is grouping into categories, but the next logical step (as it is labeled data) is classification.
        * Any regressors: this next logical step is classification because I want to predict a category.
7. Tuning the top classifiers:
    * GBC (poi_id.py --gbc): This was the first and most successful candidate.
        * Reported scores: (F1: .461; precision: .529; recall: .5)
        * Test results: (F1: .38; precision: .327; recall: .453)
    * DTC (poi_id.py --dtc): This was the second most promising.
        * Reported scores: (F1: .569; precision: .62; recall: .6)
        * Test results: (F1: .306; precision: .0.298; recall: .0.315)
    * SVC (poi_id.py --svc): This was included because decent results where expected even though the model selection showed otherwise.
        * Reported scores: (F1: .392; precision: .34; recall: .534)
        * Test results: (F1: .319; precision: .297; recall: .345)
    * RFC (poi_id.py --rfc): This was at least as promising as the DTC, but due to the time it took to optimize this classifier with possibly on par results, it was cancelled and has no scores to report. (Running the script in read-only will result in errors.)
8. Looking back at what I could have done better:
    * Plan ahead more:
        * I spent 6h+/day for 3 straight days on something that had no use.
    * Finish course ud170 first:
        * I could probably have increased the results at least a bit by improving on feature selection/creation and making more of an effort on understanding the data (I had the mentality of K-Best will figure this out).
    * Using GaussianNB on email data:
        * It was not possible to evaluate this so this is something I may come back to. It was not in the scope for this project judging by the tester.

## Answers to project questions

1. Summarize for us the goal of this project and how machine learning is useful in trying to accomplish it. As part of your answer, give some background on the dataset and how it can be used to answer the project question. Were there any outliers in the data when you got it, and how did you handle those?  [relevant rubric items: “data exploration”, “outlier investigation”]
    * The goal of this project is to identify POIs of the Enron case. ML is a good tool for this as it is hard and time-consuming to find the right indicators using classic methods. In the least ML can be a tool to back up someone's findings.
    * The dataset contains financial data and email data of people officially associated with Enron. This may have many relevant indicators as both financial data and email data may reveal enough to identify a POI or raise enough suspicions to identify someone as POI. This data was also used in the case.
    * Their where a couple outliers. The most important one was TOTAL and a couple entries that contained no or barely any data. These are the ones I removed. Outliers can also be found when you look per feature for one, but these outliers where more useful than harmful.
2. What features did you end up using in your POI identifier, and what selection process did you use to pick them? Did you have to do any scaling? Why or why not? As part of the assignment, you should attempt to engineer your own feature that does not come ready-made in the dataset -- explain what feature you tried to make, and the rationale behind it. (You do not necessarily have to use it in the final analysis, only engineer and test it.) In your feature selection step, if you used an algorithm like a decision tree, please also give the feature importances of the features that you use, and if you used an automated feature selection function like SelectKBest, please report the feature scores and reasons for your choice of parameter values.  [relevant rubric items: “create new features”, “intelligently select features”, “properly scale features”]
    * The features that made it in the end where financial features not containing the keyword *deferred* as they had a negative impact and email features not containing text and from & to message count as those are used to create new features and have little information gain. Features containing little support are also removed. (I could have compared features by looking what is common between pois and what is different between them and non-POIs.)
    * I used a MinMaxScaler to do the scaling. I used scaling as I was using classifiers like SVC.
    * The features I engineered myself where ratios of messages to/from POIs with what is sent/received and between_poi which has a high value when from/to POI count are both high. It seemed likely to me that POIs interact a lot with each other even though others may interact a lot to, possibly affecting precision or the general score. (I could have used a formula like F1 or measure the count.)
    * I used SelectKBest in a pipeline so that every specific classifier is tested with a different feature selection process. In hindsight I could have tested if this affects results a lot more than running this once and use the same k on all classifiers.
3. What algorithm did you end up using? What other one(s) did you try? How did model performance differ between algorithms?  [relevant rubric item: “pick an algorithm”]
    * I tested the GradientBoostingClassifier, SVC, DecisionTreeClassifier and tried using the RandomForestClassifier. The optimization of RFC was cancelled as it would've taken multiple days the way I set it up. Of all these GBC was the best in the test with DTC reporting the highest theoretical scores. SVC was a bit disappointing as I expected a lot from it after reading the sklearn map and the reasoning behind it. However, it has a precision of around .3 and a decent recall & F1 score.
4. What does it mean to tune the parameters of an algorithm, and what can happen if you don’t do this well?  How did you tune the parameters of your particular algorithm? What parameters did you tune? (Some algorithms do not have parameters that you need to tune -- if this is the case for the one you picked, identify and briefly explain how you would have done it for the model that was not your final choice or a different model that does utilize parameter tuning, e.g. a decision tree classifier).  [relevant rubric items: “discuss parameter tuning”, “tune the algorithm”]
    * Tuning the parameters is crucial if you want to prevent overfitting and if you want a generally better performing classifier.
    * I used an exhaustive grid search with more than good enough parameter ranges taking a lot of time. Every iteration I also did preprocessing which was probably not necessary adding a more time than the parameter tuning in some cases.
    * Every time deciding on what parameters to tune I read the documentation of sklearn in great detail. From this I often did not only know what parameters to tune, but I often learned something about other classifiers or preprocessing. For a deeper look into what **parameter ranges** I used you can take a look at ml_logger.py or when looking for the **used parameters** run the poi_id script in read only.
5. What is validation, and what’s a classic mistake you can make if you do it wrong? How did you validate your analysis?  [relevant rubric items: “discuss validation”, “validation strategy”]
    * You can mix up training and testing in way too many ways. You could simply be switching them up when predicting/fitting, you could have a leakage of your test set into the train set when doing preprocessing on a full set (e.g. scalers, imputers, kbest), you could use a not so effective cross-validation, you could validate on only train data or you could be using cross-validation to split the set and use cross-validation on the training set (this was my mistake).
6. Give at least 2 evaluation metrics and your average performance for each of them.  Explain an interpretation of your metrics that says something human-understandable about your algorithm’s performance. [relevant rubric item: “usage of evaluation metrics”]
    * I ended up using multiple metrics so that the user can decide for every classifier what metric to use even though F1 seems to be generally performing the best. I tried using my own three metrics favouring precision, F1 or recall, but I made those as I was using the grid search wrong and they turned out to be meaningless later on.
    * F1 looks at both recall and precision. If one is low F1 will be low too. This metric is great if you want to take in account both metrics. In this case I would normally prefer recall, but I choose F1 as the scores have to be higher than .3. The reason as of why I prefer recall is because I rather misidentify people as POI. This would have no real world consequence but possible waste of time for everyone.

## poi_id usage

### poi_id [--model-selection, --tune [--full] | [--prefer-difference, --read-only [scorer]]

#### Flags

* --model-selection: run the model selection
* --tune: tune the favorable classifier (quick run by default)
* --full: run the full evaluation (100 folds on model evaluation & 1000 on tuning). **This might take more than 12h!**
* --prefer-difference: try a different evaluator.
* --read-only: run from saved data.

#### Switches

##### Scorer

**options:** [--recall | --precision | --f1 | --roc_auc | --accuracy]

Choose which scorer you want to evaluate your model with. *This does not affect the training/tuning results, rather it decides how to pick the best classifier from reading those results.*

#### Tuning (howto)

Running: *python poi_id.py --tune --full --f1* will tune the favorable classifier to tune.

After running this exhaustive parameter search it will save all relevant (scores > .3) results as *classifier_tuning.pkl*. This means that it's possible to try different metrics (scorers) after running this once (use --read-only in conjunction with a scorer to perform this). *The only exception to this is when you would change the script to run a different search.*

To test your chosen model run tools/validate.py or tester.py. Both these scripts perform the same operations and don't take any arguments. *tools/validate.py has a progress bar integrated and saves to results to a file as well. tester.py is the original tester that the Udacity evaluator uses.*

#### TL;DR

Try running:

* python poi_id.py --model-selection --tune --read-only

OR

* python poi_id.py

After this you get the results from a favorable classifier (gbc) with as metric F1.

If you want to retrain the models (inaccurately but quick, using F1):

* python poi_id.py --model-selection --tune