# Report

## Commands

* python poi_id.py --tune --read-only --f1
* python poi_id.py --tune --read-only --precision
* python poi_id.py --tune --read-only --recall
* python poi_id.py --tune --read-only --f1 --prefer-difference
* python poi_id.py --tune --read-only --precision --prefer-difference
* python poi_id.py --tune --read-only --recall --prefer-difference
* python poi_id.py --tune --read-only --f1 (custom search: highest std)
* python poi_id.py --tune --read-only --f1 (custom search: lowest std)

## Results

* Accuracy: 0.74858 Precision: 0.31728 Recall: 0.44150 F1: 0.36922 F2: 0.40944
* Accuracy: 0.75108 Precision: 0.32229 Recall: 0.44750 F1: 0.37471 F2: 0.41524
* Accuracy: 0.67050 Precision: 0.26925 Recall: 0.57000 F1: 0.36574 F2: 0.46591
* Accuracy: 0.66842 Precision: 0.26613 Recall: 0.56300 F1: 0.36142 F2: 0.46031
* Accuracy: 0.67050 Precision: 0.26892 Recall: 0.56850 F1: 0.36513 F2: 0.46492
* Accuracy: 0.67042 Precision: 0.26875 Recall: 0.56800 F1: 0.36486 F2: 0.46455
* Accuracy: 0.69983 Precision: 0.28888 Recall: 0.54800 F1: 0.37832 F2: 0.46464
* Accuracy: 0.66917 Precision: 0.26670 Recall: 0.56300 F1: 0.36194 F2: 0.46064