import os
import sys
import warnings
import numpy as np
import pandas as pd
from sklearn.exceptions import UndefinedMetricWarning
from feature_format import featureFormat, targetFeatureSplit

warnings.filterwarnings("ignore", category=UndefinedMetricWarning)
rows, columns = os.popen('stty size', 'r').read().split()
pd.set_option("display.max_rows", int(rows))
pd.set_option("display.width", int(columns))

financial_features, email_features = [
    [
        "salary", "deferral_payments", "total_payments", "loan_advances", "bonus",
        "restricted_stock_deferred", "deferred_income", "total_stock_value", "expenses",
        "exercised_stock_options", "other", "long_term_incentive", "restricted_stock",
        "director_fees"
    ],
    [
        "to_messages", "from_poi_to_this_person", "from_messages",
        "from_this_person_to_poi", "shared_receipt_with_poi"
    ]
]


def select_features():
    # Load the dictionary containing the dataset
    with open("final_project_dataset.pkl", "r") as data_file:
        data_dict = pd.read_pickle(data_file)
    selected_features = financial_features + email_features
    df = pd.DataFrame.from_records(data_dict.values())[
        ["poi"] + selected_features
    ].set_index(pd.Series(data_dict.keys())).replace(to_replace="NaN", value=np.nan)
    df[selected_features].astype(float, inplace=True)
    return df


def create_features(df):
    def valid_messages():
        return (df.from_this_person_to_poi > df.to_messages).sum() +\
            (df.from_poi_to_this_person > df.from_messages).sum() == 0

    f1, f2, f3 = ["ratio_to_poi", "ratio_from_poi", "between_poi"]
    if not valid_messages():
        t1, t2 = df.to_messages, df.from_messages
        df.drop(["from_messages", "to_messages"], axis=1, inplace=True)
        df["from_messages"], df["to_messages"] = t1, t2
    df[f1] = df.from_this_person_to_poi / df.to_messages
    df[f2] = df.from_poi_to_this_person / df.from_messages
    df[f3] = df[f1] * df[f2]


def get_df_meta(df):
    return pd.Series({
        "poi": df.poi.sum(),
        "non_poi": (df.poi == False).sum(),
        "features": df.shape[1]-1,
        "min_count": df.count().min()
    })


def convert_data(df):
    # Store to dataset for easy export below.
    dataset = df.reset_index().drop("index", 1).T.to_dict()
    # Extract features and labels from dataset for local testing
    data = featureFormat(dataset, df.columns.tolist(), sort_keys=True)
    return df, dataset, targetFeatureSplit(data)


def get_cli_param(flags, switches):
    def handle_switch(key):
        switch = switches[key].apply(lambda sw: sw in sys.argv)
        if switch.sum() > 1:
            return "Multiple "+key+" switches are passed as parameter."
        elif switch.any():
            cli_param[key] = switches[key][switch].values[0][2:]
        if key not in cli_param:
            cli_param[key] = switches[key].values[0][2:]
        return cli_param

    cli_param = flags.apply(lambda x: x in sys.argv)
    if not cli_param.any():
        cli_param.model_selection = True
        cli_param.tune = True
        cli_param.read_only = True
    for key in switches.index:
        cli_param = handle_switch(key)
        if type(cli_param) is str:
            return cli_param
    return cli_param
