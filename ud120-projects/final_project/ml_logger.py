import pandas as pd
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import (AdaBoostClassifier, GradientBoostingClassifier,
                              RandomForestClassifier)
from sklearn.feature_selection import SelectKBest
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.model_selection import GridSearchCV, StratifiedShuffleSplit
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer, MinMaxScaler, PolynomialFeatures
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier


def get_scorer_name(key):
    return key[key.index("test_")+len("test_"):]


def get_clf_name(clf):
    name = str(type(clf))
    return name[name.rfind(".")+1:-2]


def filter_results(results, min=.3):
    return results[(results[[k for k in results.columns if k in [
        "mean_test_f1", "mean_test_precision", "mean_test_recall"
    ]]] > min).all(1)]


class ResultsReader(object):
    def __init__(self, filename, filter=True):
        self.results = pd.read_pickle(filename)
        if filter:
            self.results = filter_results(self.results)
        if self.results.empty:
            raise Exception("No relevant results are found (all scores < .3).")
        self.keys = pd.Series(self.results.columns).apply(
            lambda col: "mean_test_" in col or "std_test_" in col)
        self.keys = self.results.columns[self.keys].tolist()
        if "rank_test_f1" in self.results:
            self.results.sort_values("rank_test_f1", inplace=True)

    def get_best_records(self, by, scorer=None):
        items = self.results.sort_values(by)
        if scorer is None:
            return pd.Series({
                "best_params": pd.Series(items.head(1)["params"].values[0]),
                "best_scores": items.head(1)[self.keys]
            })
        return pd.DataFrame({
            "clf": items["param_clf"].apply(get_clf_name),
            scorer: items["mean_test_"+scorer]
        })

    def get_best_record_by_difference(self, scorer):
        def get_difference_std_mean(self, scorer):
            df = pd.DataFrame()
            keys, results, step = self.keys, self.results, len(self.keys)/2
            for i in range(step):
                diff = results[keys[i]] - results[keys[i+step]]
                df[get_scorer_name(keys[i])] = diff
            return df.sort_values(scorer, ascending=False)
        item = get_difference_std_mean(self, scorer).head(1)
        item = self.results.ix[item.index]
        return pd.Series({
            "best_params": pd.Series(item["params"].values[0]),
            "best_scores": item[self.keys]
        })

    def get_report(self, scorer, prefer_difference):
        if prefer_difference:
            best = self.get_best_record_by_difference(scorer)
        else:
            best = self.get_best_records("rank_test_" + scorer)
        return best


class ResultsWriter(object):
    n_jobs, verbosity = -1, 1

    @property
    def n_features(self): return len(self.features[0])

    @property
    def estimators(self): return pd.Series({
        "model_selection": Pipeline([
            ("imputer", Imputer()),
            ("polynomial", PolynomialFeatures()),
            ("scale", MinMaxScaler()),
            ("reduct", PCA(whiten=True)),
            ("clf", None)
        ]),
        "classification_tuning": Pipeline([
            ("imputer", Imputer()),
            ("select", SelectKBest()),
            ("polynomial", PolynomialFeatures()),
            ("scale", MinMaxScaler()),
            ("reduct", PCA(whiten=True)),
            ("clf", None)
        ]),
        "simple_tuning": Pipeline([
            ("imputer", Imputer()),
            ("select", SelectKBest()),
            ("scale", MinMaxScaler()),
            ("reduct", PCA(whiten=True)),
            ("clf", None)
        ])
    })

    @property
    def param_grids(self): return pd.Series({
        "model_selection": {
            "clf": [
                SVC(), GaussianNB(), DecisionTreeClassifier(),
                GradientBoostingClassifier(), AdaBoostClassifier(),
                RandomForestClassifier(), KNeighborsClassifier(),
                MLPClassifier(), GaussianProcessClassifier(RBF()),
                QuadraticDiscriminantAnalysis()
            ]
        },
        "gbc": {
            "select__k": range(int(self.n_features*.75), self.n_features) + ["all"],
            "polynomial__degree": [2, 3, 4, 5],
            "clf": [GradientBoostingClassifier(presort=True, n_estimators=1500)],
            "clf__loss": ["deviance", "exponential"]
        },
        "dtc": {
            "select__k": range(int(self.n_features*.75), self.n_features) + ["all"],
            "polynomial__degree": [2, 3, 4, 5],
            "clf": [DecisionTreeClassifier(presort=True)],
            "clf__criterion": ["gini", "entropy"],
            "clf__splitter": ["best", "random"]
        },
        "rfc": {
            "select__k": range(int(self.n_features*.75), self.n_features) + ["all"],
            "polynomial__degree": [2, 3, 4, 5],
            "clf": [RandomForestClassifier(n_jobs=-1)],
            "clf__criterion": ["gini", "entropy"],
            "clf__n_estimators": [10, 50, 150]
        },
        "svc": {
            "select__k": range(int(self.n_features*.75), self.n_features) + ["all"],
            "reduct__n_components": [3, 4, 5],
            "clf": [SVC()],
            # "clf__C": [.01, .1, 1, 10, 100, 300, 600, 1000, 5000],
            "clf__C": [3000, 5000, 8000, 10000],
            "clf__gamma": [.01, .05, .1, .5],
            "clf__kernel": ["rbf", "poly"]
        }
    })

    def __init__(self, filename, data, n_splits=100):
        self.filename = filename
        self.labels, self.features = data
        self.n_splits = n_splits

    def run_write_model_selection(self):
        return self.run_write_grid_search(
            estimator=self.estimators.model_selection,
            param_grid=self.param_grids.model_selection
        )

    def run_write_classification_tuning(self, param_grid_idx, filter=True):
        return self.run_write_grid_search(
            estimator=self.estimators.simple_tuning if param_grid_idx == "svc"
            else self.estimators.classification_tuning,
            param_grid=self.param_grids[param_grid_idx],
            filter=filter
        )

    def run_write_grid_search(self, estimator, param_grid, filter=False):
        grid = GridSearchCV(
            estimator=estimator,
            param_grid=param_grid,
            scoring=("accuracy", "f1", "recall", "precision", "roc_auc"),
            error_score=0,
            verbose=self.verbosity,
            n_jobs=self.n_jobs,
            return_train_score=True,
            cv=StratifiedShuffleSplit(n_splits=self.n_splits),
            refit="f1"
        ).fit(self.features, self.labels)
        results = pd.DataFrame(grid.cv_results_)
        if filter:
            results = filter_results(results)
        results.to_pickle(self.filename)
        return self.filename
