#!/usr/bin/python

import sys
sys.path.append("../tools/")
import numpy as np
import pandas as pd
from helper import (convert_data, create_features, get_cli_param, get_df_meta,
                    select_features)
from ml_logger import ResultsReader, ResultsWriter
from tester import dump_classifier_and_data

flags = pd.Series({
    "model_selection": "--model-selection",
    "tune": "--tune",
    "read_only": "--read-only",
    "prefer_difference": "--prefer-difference",
    "full": "--full"
})
switches = pd.Series({
    "scorer": pd.Series(["--f1", "--precision", "--recall", "--roc_auc", "--accuracy"]),
    "classifier": pd.Series(["--gbc", "--dtc", "--rfc", "--svc"])
})
cli_param = get_cli_param(flags, switches)
if type(cli_param) is str:
    print cli_param
    sys.exit()
print "\nFor poi_id usage read README.md\n"
# Task 1: Select what features you'll use.
df = select_features()
# Task 2: Remove outliers
print "="*80
print "Preping the dataset"
print "="*80
df.drop("TOTAL", inplace=True)
tagged = df.isna().sum(1) > (df.shape[1]-1)*.69
print "Deleted entries:", len(df[tagged])
print "POIs missing:", len(df[tagged]) - len(df[tagged & ~df.poi])
df.drop(df[tagged].index, inplace=True)
# Task 3: Create new feature(s)
create_features(df)
print "Created feature(s): 2"
tagged = df.columns[df.count() < 10].tolist() + [
    "deferral_payments", "deferred_income", "restricted_stock_deferred",
    "from_messages", "to_messages"
]
print "Deleted feature(s):", len(tagged)
df.drop(tagged, 1, inplace=True)
print "\nDataset info:\n", get_df_meta(df)
# Task 4: Try a varity of classifiers
df, dataset, data = convert_data(df)
scorer, classifier = cli_param.scorer, cli_param.classifier
if cli_param.model_selection:
    print "="*80
    print "Trying a varity of classifiers"
    print "="*80
    n_splits = 100 if cli_param.full else 10
    writer = ResultsWriter("model_selection.pkl", data, n_splits)
    reader = ResultsReader("model_selection.pkl" if cli_param.read_only
                           else writer.run_write_model_selection(), False)
    print reader.get_best_records("rank_test_"+scorer, scorer)
if not cli_param.tune:
    sys.exit()
# Task 5: Tune your classifier to achieve better than .3 precision and recall
print "="*80
print "Tuning the classifier to achieve better results"
print "="*80
n_splits = 1000 if cli_param.full else 10
writer = ResultsWriter(classifier + "_tuning.pkl", data, n_splits)
print "Classifier:", classifier, "\n"
reader = ResultsReader(classifier + "_tuning.pkl" if cli_param.read_only
                       else writer.run_write_classification_tuning(classifier))
best = reader.get_report(scorer, cli_param.prefer_difference)
tag = "["+scorer+(" mean-std" if cli_param.prefer_difference else "")+"][best]"
print 5*"-", tag, "parameters", 5*"-", "\n"+str(best.best_params.drop("clf"))
print 5*"-", tag, "scores", 5*"-", "\n"+str(best.best_scores)
clf = writer.estimators.classification_tuning.set_params(**best.best_params)

# Task 6: Dump your classifier, dataset, and features_list
dump_classifier_and_data(clf, dataset, df.columns.tolist())
