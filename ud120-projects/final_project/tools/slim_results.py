import sys
sys.path.append("..")
import pandas as pd
from ml_logger import filter_results

filename = sys.argv[1]
results = pd.read_pickle(filename)
results.to_pickle(filename+".bak")
filter_results(results).to_pickle(filename)