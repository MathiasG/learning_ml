#!/usr/bin/python

""" 
    Starter code for exploring the Enron dataset (emails + finances);
    loads up the dataset (pickled dict of dicts).

    The dataset has the form:
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }

    {features_dict} is a dictionary of features associated with that person.
    You should explore features_dict as part of the mini-project,
    but here's an example to get you started:

    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000
    
"""

import pickle

enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))
persons = enron_data.keys()
print "People count:", len(persons)
print "Features count:", len(enron_data[persons[0]])
print "POI count:", sum(1 for p in persons if enron_data[p]['poi'])
print "POI names:", (sum(1 for columns in ( raw.strip().split() for raw in open("../final_project/poi_names.txt", "r") )) - 2)
print "Info about James Prentice:", repr(enron_data["PRENTICE JAMES"])
print "Info about Wesley Colwell:", repr(enron_data["COLWELL WESLEY"])
print "Info about Jeffrey K Skilling:", repr(enron_data["SKILLING JEFFREY K"])
print "Total payments SKILLING JEFFREY K:", enron_data["SKILLING JEFFREY K"]["total_payments"]
print "Total payments LAY KENNETH L:", enron_data["LAY KENNETH L"]["total_payments"]
print "Total payments FASTOW ANDREW S:", enron_data["FASTOW ANDREW S"]["total_payments"]
print "Quantified salaries:", sum(1 for p in persons if enron_data[p]["salary"] != "NaN")
print "Known emails:", sum(1 for p in persons if enron_data[p]["email_address"] != "NaN")
payments = sum(1 for p in persons if enron_data[p]["total_payments"] != "NaN")
print "Quantified total payments:", payments
print "Percentile of NaN as total payments:", str(100. * (len(persons) - payments) / len(persons)) + "%"
paymentsPoi = sum(1 for p in persons if enron_data[p]["poi"] and enron_data[p]["total_payments"] != "NaN")
print "Quantified total payments for POIs:", paymentsPoi
print "Percentile of NaN as total payments for POIs:", str(100. * (len(persons) - paymentsPoi) / len(persons)) + "%"